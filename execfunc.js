const helpers = require('./helpers.js') 

process.on('message', (functionDefinition) => {

  try {

    let f = new Function('params', 'helpers', `
      "use strict"
      console.log("🤖 function initialization")
      let lambda = ${functionDefinition.funktion}
      return lambda(params)
    `)
    
    const res = f(functionDefinition.params, helpers)
    process.send(res);
  } catch(err) {
    console.log("😡", err.message)
    process.send(err.message);
  }
});