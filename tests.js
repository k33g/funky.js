const testIt = require('./test-it')

let f = testIt.getFunctionFrom("./demo-function.js")

/*
testIt.assert("42 is 42", 42 == 42)
testIt.assert("0 is 0", 0 === 0)
*/

testIt.assert("sam cmd", f({name:"sam"})[0] == "salade")
testIt.assert("sam cmd", f({name:"sam"})[1] == "tomates")
testIt.assert("sam cmd", f({name:"sam"})[2] == "veau")



testIt.end()