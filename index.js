const express = require('express')
const { fork } = require('child_process')

const app = express()
const port = process.env.PORT || 9090

// http://2ality.com/2015/01/es6-maps-sets.html
let functions = new Map()

// http://localhost:9090/function/hello/1.0.0/{"name":"Bob Morane"}


functions.set("hello:1.0.0", `
  (params) => {
    return "<h1>👋 " + helpers.yo() + " " + params.name + "</h1>"
  }
`)

functions.set("morgen:1.0.0", `
  (params) => {
    return "<h1>👋 morgen</h1>"
  }
`)

app.use(express.static('public'))

function killProcess(forkedProcess, res) {
  console.log(`forkedProcess was => ${forkedProcess}`);
  try {
    forkedProcess.kill()
    res.json({error:"process too long, I killed it"})
  } catch (error) { // check if error.code == ERR_HTTP_HEADERS_SENT
    if(error.code=="ERR_HTTP_HEADERS_SENT") {
      console.log("🤔 nothing to kill", error.message, error.code)
    } else {
      console.log("😡", error.message, error.code)
    }
  }
}

app.use(express.json());

let getFunction = (req, res) => {
  let funktion = functions.get(`${req.params["name"]}:${req.params["version"]}`)
  let functionCompute = fork('execfunc.js')
  //https://nodejs.org/api/process.html#process_process_send_message_sendhandle_options_callback
  //The message goes through serialization and parsing. The resulting message might not be the same as what is originally sent.
  functionCompute.send({
    params: JSON.parse(req.params["params"]),
    funktion: funktion
  }) 
  // kill the forked process if it runs during more than n milliseconds
  setTimeout(killProcess, process.env.DELAY || 1500, functionCompute, res);

  return functionCompute
}

// add or update function
app.post('/function', (req, res) => {
  //TODO: try catch
  console.log(req.body.source)
  functions.set(`${req.body.name}:${req.body.version}`, req.body.source)
  res.send({message:`${req.body.name}:${req.body.version} added`})
})

// Remove a function
// curl -X DELETE http://localhost:9090/function/hello/1.0.0
app.delete('/function/:name/:version', (req, res) => {
  //TODO: try catch
  functions.delete(`${req.params["name"]}:${req.params["version"]}`)
  res.send({message:`${req.params["name"]}:${req.params["version"]} deleted`})
})

app.get('/functions', (req, res) => {
  res.json(Array.from(functions.keys()))
})

// get function result as json
app.get('/function/:name/:version/:params', (req, res) => {
  getFunction(req, res).on('message', result => {
    res.json({result: result})
  })
})

// get function result as html
app.get('/function/html/:name/:version/:params', (req, res) => {
  res.set('Content-Type', 'text/html')
  getFunction(req, res).on('message', result => {
    res.send(result)
  })
})

// get function result as text
app.get('/function/text/:name/:version/:params', (req, res) => {
  res.set('Content-Type', 'text/plain')
  getFunction(req, res).on('message', result => {
    res.send(result)
  })
})

app.listen(port, () => console.log(`🌍 funky-js is listening on port ${port}!`))