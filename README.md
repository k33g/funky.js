# Funky.js

## Why?

a quick'n dirty function platform to investigate with GitLab CI/CD

## Start Funky.js

```shell
npm start
```

## Write a function

Create a JavaScript file `./functions/demo-function.js`

```javascript
(params) => {
  let name = params.name
  console.log("name", name)
  let cmds = {
    "bob": ["salade", "tomates", "oignons", "veau", "harissa"],
    "sam": ["salade", "tomates", "veau"]
  }
  return cmds[name]
}
```

## Add a function

```shell
chmod +x deploy
./deploy yo tada ./functions/demo-function.js localhost 9090
```

## Remove a function

```shell
chmod +x remove
./remove yo tada localhost 9090
```

## Run a function (returning JSON)

call http://localhost:9090/function/yo/tada/{"name":"bob"}

> where `yo` is the name of the function, `tada` is the "version" of the function

### If your function return HTML

call http://localhost:9090/function/html/yo/tada/{"name":"bob"}

### If your function return TEXT

call http://localhost:9090/function/text/yo/tada/{"name":"bob"}

## Deploy Funky.js to Clever Cloud

See the `.gitlab-ci.yml` file of this repository

## Use it with GitLab CI

### First

- create a project with your function file (`demo-function.js`)
- add these files to the project: `deploy` and `remove`

### Create a file named `.gitlab-ci.yml`

```yaml
image: node:latest

stages:
  - 🚀deploy
  - 🦄preview

variables:
  PLATFORM_DOMAIN: "funky.js.cleverapps.io" # this is the domain to reach the "faas" platform
  PLATFORM_HTTP_PORT: 80
  FUNCTION_FILE: "./demo-function.js"

before_script:
  - chmod +x deploy
  - chmod +x remove

deploy:master:
  stage: 🚀deploy
  only:
    - master
  script: |
    ./deploy $CI_PROJECT_NAME master $FUNCTION_FILE $PLATFORM_DOMAIN $PLATFORM_HTTP_PORT
  environment:
    name: production/$CI_PROJECT_NAME
    url: http://$PLATFORM_DOMAIN:$PLATFORM_HTTP_PORT/function/$CI_PROJECT_NAME/master/{"name":"bob"}


deploy:mr:
  stage: 🦄preview
  only:
    - merge_requests
  script: |
    ./deploy $CI_PROJECT_NAME $CI_COMMIT_REF_SLUG $FUNCTION_FILE $PLATFORM_DOMAIN $PLATFORM_HTTP_PORT
  environment:
    name: review/$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG
    url: http://$PLATFORM_DOMAIN:$PLATFORM_HTTP_PORT/function/$CI_PROJECT_NAME/$CI_COMMIT_REF_SLUG/{"name":"bob"}
    on_stop: stop_review

stop_review:
  stage: 🦄preview
  when: manual
  only:
    - merge_requests
  script: |
    ./remove $CI_PROJECT_NAME $CI_COMMIT_REF_SLUG $PLATFORM_DOMAIN $PLATFORM_HTTP_PORT
  environment:
    name: review/$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG
    action: stop
```

## You want to add some tests:

### First

- add these files to your project: `test-it.js` and `tests.js`

### Create a file named `package.json`

```json
{
  "name": "hello-function",
  "scripts": {
    "test": "node tests.js"
  }
}
```

### Add a tests stage to `.gitlab-ci.yml` file

```yaml
stages:
  - 🚀deploy
  - 🤞tests
  - 🦄preview

```

### Add a test job to `.gitlab-ci.yml` file

```yaml
test:mr:
  stage: 🤞tests
  only:
    - merge_requests
  script: |
    npm test
```